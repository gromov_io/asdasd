import axios, { AxiosRequestConfig, AxiosInstance, AxiosResponse } from 'axios'

const instance: AxiosInstance = axios.create({
	baseURL: 'http://localhost:5137/api/v1',
	headers: {
		'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
	}
})

const Request = (config: AxiosRequestConfig) => {
	const token = localStorage.getItem('npd_token')
	if (token) {
		config.headers['Authorization'] = `Bearer ${token}`
	}
	return config
}

const RequestError = (error: any) => {
	return Promise.reject(error)
}

const Response = (response: AxiosResponse<any>) => {
	return response
}

const ResponseError = (error: any) => {
	return Promise.reject(error)
}

instance.interceptors.request.use(Request, RequestError)
instance.interceptors.response.use(Response, ResponseError)

export default instance