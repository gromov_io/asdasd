import { changeMediaQuery } from '~/store/redux/modules/MediaQuery'
import { debounce } from 'lodash-es'
const isMobile = window.matchMedia('(max-width: 767px)')
const isTablet = window.matchMedia('(min-width: 768px)')

export function matchMedia(dispatch:any) {
	dispatch(changeMediaQuery({ 
		isMobile: isMobile.matches,
		isTable: isTablet.matches,
		isDesktop: false
	}))
	
	const changeQuery = debounce(function() {
		dispatch(changeMediaQuery({
			isMobile: isMobile.matches,
			isTable: isTablet.matches,
			isDesktop: false
		}))
	}, 100)

	isMobile.addListener(changeQuery)
	isTablet.addListener(changeQuery)
}