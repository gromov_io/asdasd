import React from 'react'

export const Nalog = (props) => {
	return <svg xmlns="http://www.w3.org/2000/svg"  xlink="http://www.w3.org/1999/xlink" viewBox="0 0 23 15.333">
		<defs>
			<clipPath id="a">
				<path fill="currentColor" d="M15.69,15.158a1.215,1.215,0,0,0,1.725,0l9.3-9.3a.421.421,0,0,0,.1-.479c-.1-.192-.192-.383-.383-.383H6.682c-.192,0-.287.192-.383.383a.421.421,0,0,0,.1.479Z" transform="translate(-6.259 -5)" />
			</clipPath>
			<clipPath id="b">
				<path fill="currentColor" d="M27.9,6.8h-.192l-5.942,5.942a.291.291,0,0,0,0,.383l4.792,4.792a.635.635,0,0,1,.192.671c-.1.287-.287.479-.479.479a.593.593,0,0,1-.671-.192l-4.792-4.792a.335.335,0,0,0-.192-.1c-.1,0-.1,0-.192.1L18.417,16.1a2.6,2.6,0,0,1-3.738,0l-2.013-2.012a.291.291,0,0,0-.383,0L7.492,18.875a.745.745,0,0,1-1.054-1.054l4.792-4.792a.291.291,0,0,0,0-.383L5.288,6.9c-.1-.1-.1-.1-.192,0-.1,0-.1,0-.1.1v11.5a1.922,1.922,0,0,0,1.917,1.917H26.083A1.922,1.922,0,0,0,28,18.492V6.992C28,6.9,28,6.9,27.9,6.8Z" transform="translate(-5 -6.8)" />
			</clipPath>
		</defs>
		<g transform="translate(-5 -5)">
			<g transform="translate(6.206 5)">
				<g clipPath="url(#a)" transform="translate(0)">
					<rect fill="currentColor" width="30.092" height="20.029" transform="translate(-4.752 -4.792)" />
				</g>
			</g>
			<g transform="translate(5 6.725)">
				<g clipPath="url(#b)" transform="translate(0 0)">
					<rect fill="currentColor" width="32.583" height="23.192" transform="translate(-4.792 -4.792)" />
				</g>
			</g>
		</g>
	</svg>
}

export default Nalog