import React from 'react'

export const Nalog = () => {
	return <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.466 20.127">
		<g transform="translate(0.5 0.5)">
			<path fill="currentColor" stroke="currentColor" d="M450.866,85.269V75.111h4.316c2.275,0,3.516.233,4.782,1.138a5.717,5.717,0,0,1,0,8.582c-1.24.956-2.61,1.137-4.782,1.137h-3.541v2.766h6.488v.7h-6.488v4.807h-.775V89.431h-2.43v-.7h2.43V85.967h-2.43v-.7Zm.775,0h3.566c2.146,0,3.258-.258,4.343-1.06a5.059,5.059,0,0,0,0-7.341c-1.085-.8-2.2-1.06-4.343-1.06h-3.566Z" transform="translate(-448.436 -75.111)" />
		</g>
	</svg>
}

export default Nalog