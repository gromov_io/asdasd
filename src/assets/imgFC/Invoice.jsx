import React from 'react'

export const Invoice = () => {
	return <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 20.842">
		<path fill="currentColor" d="M17.714,0H2.286A1.223,1.223,0,0,0,1,1.3V20.842l3.857-2.605,2.571,2.605L10,18.237l2.571,2.605,2.571-2.605L19,20.842V1.3A1.223,1.223,0,0,0,17.714,0ZM15.143,13.026H4.857V10.421H15.143Zm0-5.211H4.857V5.211H15.143Z" transform="translate(-1)" />
	</svg>
}

export default Invoice