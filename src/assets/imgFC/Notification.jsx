import React from 'react'

export const Notification = (props) => {
	return (<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21 23">
	    <g transform="translate(-1395 -20)">
	        <path fill="currentColor" d="M-1371,81h4a2.006,2.006,0,0,1-2,2A2.006,2.006,0,0,1-1371,81Zm-5-1a.945.945,0,0,1-1-1,.945.945,0,0,1,1-1h.5a4.354,4.354,0,0,0,1.5-3V72a4.952,4.952,0,0,1,5-5,4.951,4.951,0,0,1,5,5v3a4.351,4.351,0,0,0,1.5,3h.5a.945.945,0,0,1,1,1,.945.945,0,0,1-1,1Z" transform="translate(2772 -40)" />
	        <g fill="#e35f50" stroke="#fff" strokeWidth="2px" transform="translate(1406 22)">
	            <circle stroke="none" cx="4" cy="4" r="4" />
	            <circle fill="none" cx="4" cy="4" r="5" />
	        </g>
	    </g>
	</svg>)
}

export default Notification