import React, { FC } from 'react'
import { IHomeProps } from './Home.model'
import { Button } from 'antd'
import { Collapse } from 'antd'

const { Panel } = Collapse

export const Home:FC<IHomeProps> = () => {
	return (
		<div className="page home">
			<h1 className="page__title">Главная</h1>
			
			<div className="page__container">
				<div className="home__section">
					<div className="home__section-wrap">
						<h2 className="h2 home__title">Доход за сентябрь</h2>
						<div className="sum _high _green home__sum">11 650,00 <span className="sum__currency">₽</span></div>
					</div>
					<div className="home__section-wrap _btn">
						<Button type="primary" className="_default home__btn">Новая продажа</Button>
					</div>
				</div>
				<div className="home__section">
					<div>
						<div className="h3">Предварительный налог</div>
						<div className="sum _middle">11 650,00 <span className="sum__currency">₽</span></div>
						<div>Полная сумма налога за текущий месяц станет известна до 12-го числа следующего месяца.</div>
						<Collapse bordered={false} expandIconPosition="right">
							<Panel header="Подробнее" key="1" className="home__collapse">
								<div>
									<span className="sum home__collapse-sum">650,00 <span className="sum__currency">₽</span></span>
									<span>физ. лица (4%) за 5 операций</span>
								</div>
								<div>
									<span className="sum home__collapse-sum">650,00 <span className="sum__currency">₽</span></span>
									<span>физ. лица (4%) за 5 операций</span>
								</div>
							</Panel>
						</Collapse>
					</div>
				</div>
				<div className="home__section">
					<div className="home__section-wrap">
						<div className="h3">Задолженность</div>
						<div className="sum _middle">650,00 <span className="sum__currency">₽</span></div>
						<div className="color-red home__debt-text">Срок уплаты – до 25.09.2019</div>
					</div>
					<div className="home__section-wrap _btn">
						<Button type="primary" className="_default home__btn">Оплатить налог</Button>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Home