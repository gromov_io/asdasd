import React , { FunctionComponent }from 'react'
import { IAccountProps } from './Account.model'
import { Button } from 'antd'
import UserDefault from '~/assets/img/Group1167.svg'
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { IStore } from '~/store'
import { useSelector, shallowEqual} from 'react-redux'

const mapStateProfile = (state: IStore) => ({
	...state.profile
})

export const Sale: FunctionComponent<IAccountProps> = () => {
	const profile = useSelector(mapStateProfile, shallowEqual)

	const user = {
		get name() {
			const firstName = profile.UserAttributes.find(item => item.Name === 'FirstName')
			const lastName = profile.UserAttributes.find(item => item.Name === 'LastName')
			const Patronymic = profile.UserAttributes.find(item => item.Name === 'Patronymic')
			return `${lastName ? lastName.Value : ''} ${(firstName ? firstName.Value[0] : '') + '.'} ${Patronymic ? Patronymic.Value[0] : ''}`
		}
	}

	return <div className="page account">
		<h1 className="page__title">Профиль</h1>

		<div className="page__container">
			<div className="account__block _head">
				<img className="account__use-image" src={UserDefault} alt=""/>
				<div className="account__user-name-wrap">
					<div className="account__user-name">{user.name}</div>
					<div className="account__grey-text _status">Статус</div>
					<div className="account__user-status-value">{profile.StatusSmz}</div>
				</div>
			</div>
			<div className="account__block">
				<div className="account__block-title">Мой кошелек</div>
				<div className="account__item">
					<div className="account__grey-text">Номер кошелька</div>
					<div className="account__value-wrap">
						<div className="account__item-value">123123123123</div>
						<CopyToClipboard text={'123123123123'} >
							<span className="account__item-action _copy">Скопировать</span>
						</CopyToClipboard>
					</div>
				</div>
				<div className="account__item">
					<div className="account__grey-text">Псевдоним</div>
					<div className="account__item-value">79164200111</div>
				</div>
			</div>
			<div className="account__block">
				<div className="account__block-title">Контактные данные</div>
				<div className="account__item">
					<div className="account__grey-text">E-mail</div>
					<div className="account__value-wrap">
						<div className="account__item-value">sergey@gromov.io</div>
						<div className="account__item-action">Подтвержден</div>
					</div>
				</div>
				<div className="account__item">
					<div className="account__grey-text">Номер телефона</div>
					<div className="account__value-wrap">
						<div className="account__item-value">+7 (916) 420 01 11</div>
						<div className="account__item-action _active">Подтвержден</div>
					</div>
				</div>
				<div className="account__item">
					<div className="account__grey-text">Skype</div>
					<div className="account__item-value"></div>
				</div>
				<div className="account__item">
					<div className="account__grey-text">Viber</div>
					<div className="account__item-value"></div>
				</div>
			</div>
			<div className="account__block">
				<div className="account__block-title">Технические и правовые ссылки</div>
				<div className="account__link-wrap">
					<a href="https://walletone.com/" className="account__link _guide">Руководство пользователя</a>
					<a href="https://walletone.com/" className="account__link _fns">Список требований ФНС</a>
					<a href="https://walletone.com/" className="account__link _lock">Права партнера W1</a>
				</div>
				<Button type="primary" className="_black account__btn">Отозвать привязку СМЗ</Button>
			</div>
		</div>
	</div>
}

export default Sale