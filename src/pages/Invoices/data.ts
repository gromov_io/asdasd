import { IInvoiceItemData } from '../../components/InvoiceItem/InvoiceItem.model'

export const Data: IInvoiceItemData[] = [
	{
		status: 'wait',
		date: '01.09.2019',
		sum: 2700,
		desc: '1 урока математики в сентябре в сентябре в сентябре',
		name: 'Иванов И. (2 класс)'
	},
	{
		status: 'success',
		date: '01.09.2019',
		sum: 2700,
		desc: '2 урока математики в сентябре в сентябре в сентябре',
		name: 'Иванов И. (2 класс)'
	},
	{
		status: 'delete',
		date: '01.09.2019',
		sum: 2700,
		desc: '2 урока математики в сентябре в сентябре в сентябре',
		name: 'Иванов И. (2 класс)'
	},
	{
		status: 'wait',
		date: '01.09.2019',
		sum: 2700,
		desc: '2 урока математики в сентябре в сентябре в сентябре',
		name: 'Иванов И. (2 класс)'
	},
	{
		status: 'delete',
		date: '01.09.2019',
		sum: 2700,
		desc: '3 урока математики в сентябре в сентябре в сентябре',
		name: 'Иванов И. (2 класс)'
	}
]

export default Data