import React, { FC, useState } from 'react'
import { InvoicesProps } from './Invoices.model'
import { DatePicker, Tabs, Modal } from 'antd'
import { InvoiceItem } from '~/components/InvoiceItem/InvoiceItem'
import localeRu from '~/plugins/date-lang-ru'
import Data from './data'
import { InvoiceModal } from '~/components/InvoiceModal/InvoiceModal'

const { TabPane } = Tabs
const { RangePicker } = DatePicker

export const Invoices: FC<InvoicesProps> = () => {
	const [from, setFrom] = useState('1990-06-23')
	const [modalStatus, setModalStatus] = useState<boolean>(false)
	const [receiptStatus, setReceiptStatus] = useState<boolean>(false)
	const [to, setTo] = useState('1990-09-23')
	
	const modalChangeStatus = (value:boolean) => {
		setModalStatus(value)
	}

	const receiptChangeStatus = (value: boolean) => {
		setReceiptStatus(value)
	}

	const className = {
		get receipt () {
			const name = 'invoice-modal'
			return receiptStatus ? name.concat(' _show-receipt') : name
		}
	}

	return (
		<div className="page invoce">
			<h1 className="page__title">История операций</h1>
			<div className="invoce__date-wrap">
				<div className="invoce__date">
					<button className="invoce__date-btn">
						<span className="invoce__date-icon"></span>
						<div className="invoce__date-picker-block">
							<RangePicker 
								onChange={() => ('')} 
								className="invoce__datepicker"
								format="DD.MM.YYYY"
								locale={localeRu}
							/>
						</div>
						<div className="invoce__date-picker-mobile">
							<input type="date" className="invoce__date-picker-input" value={from} onChange={e => setFrom(e.target.value)} />
							<span className="invoce__date-separator"> - </span>
							<input type="date" className="invoce__date-picker-input" value={to} onChange={e => setTo(e.target.value)} />
						</div>
					</button>
				</div>
				<div className="invoce__search">
				</div>
			</div>

			<div className="page__container">
				<Tabs defaultActiveKey="1" onChange={() => { }} animated={false}>
					<TabPane tab="Все" key="1">
						{Data.map((item, key) => {
							return <InvoiceItem data={item} key={key} onClick={modalChangeStatus} />
						})}
					</TabPane>
					<TabPane tab="Оплаченные" key="2">
						{Data.filter(item => item.status === 'success').map((item, key) => {
							return <InvoiceItem data={item} key={key} onClick={modalChangeStatus} />
						})}
					</TabPane>
					<TabPane tab="Неоплаченные" key="3">
						{Data.filter(item => item.status === 'delete').map((item, key) => {
							return <InvoiceItem data={item} key={key} onClick={modalChangeStatus} />
						})}
					</TabPane>
				</Tabs>
			</div>

			<Modal
				wrapClassName={className.receipt}
				title={null}
				visible={modalStatus}
				onOk={() => {}}
				onCancel={() => modalChangeStatus(false)}
				footer={null}
			>
				<InvoiceModal receiptStatus={receiptStatus} receiptChangeStatus={receiptChangeStatus} />
			</Modal>
		</div>
	)
}

export default Invoices