import { RouteComponentProps } from 'react-router-dom'

export interface IMessagesProps extends RouteComponentProps {

}

export interface IMessageItem {
	id: number
	owner: string
	title: string
	description: string
	date: string
	image: string
	status: 'no-read' | 'read'
}