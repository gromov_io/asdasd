import React, { FC } from 'react'
import { IMessagesProps } from './Messages.model'
import { shallowEqual, useSelector } from 'react-redux'
import { MessageItemsMobile } from '~/components/MessageItemsMobile/MessageItemsMobile'
import { MessageItemsDesktop } from '~/components/MessageItemsDesktop/MessageItemsDesktop'
import { IStore } from '~/store'
import { Tabs } from 'antd'
import Data from './data'

const { TabPane } = Tabs

export const Messages: FC<IMessagesProps> = () => {
	const mapState = (state: IStore) => ({
		...state.mediaQuery
	})
	const MediaQuery = useSelector(mapState, shallowEqual)
	const DataRead = Data.filter(item => item.status === 'read')
	const DataNoRead = Data.filter(item => item.status === 'no-read')

	return (
		<div className="page message">
			<h1 className="page__title">Уведомления</h1>

			<div className="page__container">
				<Tabs defaultActiveKey="1" onChange={() => { }} animated={false}>

					<TabPane tab="Все" key="1">
						{!MediaQuery.isTable
							? <MessageItemsMobile data={Data} />
							: <MessageItemsDesktop data={Data} />
						}
					</TabPane>

					<TabPane tab="Не прочитанные" key="2">
						{!MediaQuery.isTable
							? <MessageItemsMobile data={DataRead} />
							: <MessageItemsDesktop data={DataRead} />
						}
					</TabPane>

					<TabPane tab="Прочитанные" key="3">
						{!MediaQuery.isTable
							? <MessageItemsMobile data={DataNoRead} />
							: <MessageItemsDesktop data={DataNoRead} />
						}
					</TabPane>
					
				</Tabs>
			</div>
		</div>
	)
}

export default Messages