import React, { FunctionComponent, useEffect } from 'react';
import { Main } from './layouts/Main/Main'
import { BrowserRouter as Router } from 'react-router-dom'
import { Login } from './layouts/Login/Login'
import { matchMedia } from './plugins/matchMedia'
import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { IStore } from './store'

const mapStateAuth = (state: IStore) => ({
	storeAuth: state.auth
})

const App: FunctionComponent = () => {
	const { storeAuth } = useSelector(mapStateAuth, shallowEqual)
	const dispatch = useDispatch()

	useEffect(() => {
		matchMedia(dispatch)
	}, [])

	return (
		<div className="App">
			{storeAuth.authorized
				? <Router>
						<Main />
					</Router>
				: <Login />
			}
		</div>
	)
}

export default App
