import React, { FunctionComponent } from 'react'
import { IInvoiceModalProps } from './InvoiceModal.model'
import { Button, Icon } from 'antd'

export const InvoiceModal: FunctionComponent<IInvoiceModalProps> = (props) => {
	return (
		<div className="invoice-modal__content">
			<div className="invoice-modal__body">
				<div className="invoice-modal__body-wrap">
					<div className="invoice-modal__title">Операция №1731273</div>
					<div className="invoice-modal__meta-text">
						<strong>Плательщик:</strong> Столешников Андрей (8 класс)
					</div>
					<div className="invoice-modal__meta-text">
						<strong>Название услуги:</strong> Подготовка к ОГЭ
					</div>
					<div className="invoice-modal__meta-text">
						<strong>Оплата:</strong> Онлайн
					</div>
					<div className="sum _high _green invoice-modal__sum">11 650,00 <span className="sum__currency">₽</span></div>
					<div className="invoice-modal__btn-wrap">
						<Button type="primary" className="_default invoice-modal__btn">Отправить чек покупателю</Button>
					</div>
					<div className="invoice-modal__btn-wrap">
						<Button type="primary" className="_light-green invoice-modal__btn">Повторить платеж</Button>
					</div>
					<div className="invoice-modal__btn-wrap">
						<Button type="primary" className="_black invoice-modal__btn">Аннулировать чек</Button>
					</div>
					<div className="invoice-modal__show-receipt" onClick={() => props.receiptChangeStatus(!props.receiptStatus)}>
						<Icon type={props.receiptStatus ? 'eye-invisible' : 'eye'} className="invoice-modal__eye" /> {props.receiptStatus ? 'Скрыть' : 'Показать'} чек
					</div>
				</div>
			</div>
			<div className="invoice-modal__receipt">
			</div>
		</div>
	)
}

export default InvoiceModal