
export interface IInvoiceModalProps {
	receiptStatus: boolean
	receiptChangeStatus: (value:boolean)=> void
}