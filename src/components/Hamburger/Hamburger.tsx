import React, { FC } from 'react'
import { IHamburgerProps } from './Hamburger.model'
import { connect } from 'react-redux'
import { IStore } from '~/store'
import { changeStatus } from '~/store/redux/modules/Navigation'


const mapState = (state: IStore) => ({
	status: state.navigation.status
})

const mapActions = {
	changeStatus
}

const Hamburger: FC<IHamburgerProps> = (props) => {
	const className = {
		get humburger () {
			const classList = `hamburger hamburger--squeeze ${props.prefix}`
			return props.status ? classList.concat(' is-active') : classList
		}
	}

	return (
		<button className={className.humburger} type="button" onClick={() => props.changeStatus(!props.status)}>
			<span className="hamburger-box">
				<span className="hamburger-inner"></span>
			</span>
		</button>
	)
}

const _Hamburger = connect(mapState, mapActions)(Hamburger)

export { _Hamburger as Hamburger }
export default Hamburger