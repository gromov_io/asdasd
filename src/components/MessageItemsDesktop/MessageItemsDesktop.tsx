import React, { FC, useState } from 'react'
import { IMessageItemsDesktopProps, IMessageItemDesktopProps } from './MessageItemsDesktop.model'
import { Button } from 'antd'

const MessageItemDesktop: FC<IMessageItemDesktopProps> = (props) => {
	const className = {
		get item() {
			const name = 'message-item-desktop'
			return props.isActive ? name.concat(' _open') : name
		}
	}

	return <div className={className.item} onClick={() => props.setActive(props.data.id)}>
		<div className="message-item-desktop__header">
			<div className="message-item-desktop__owner-img">
				<img src={props.data.image} alt="" />
			</div>
			<div className="message-item-desktop__header-wrap">
				<div className="message-item-desktop__header-head">
					<div className="message-item-desktop__owner">{props.data.owner}</div>
					<div className="message-item-desktop__date">{props.data.date}</div>
				</div>
				<div className="message-item-desktop__title">{props.data.title}</div>
			</div>
			<div className="message-item-desktop-btn"></div>
		</div>
	</div>
}

export const MessageItemsDesktop: FC<IMessageItemsDesktopProps> = (props) => {
	const[activeId, setActiveId] = useState<Number|null>(null)
	const activeItem = props.data.find(item => item.id === activeId)

	return <div className="message-items-desktop">
		<div className="message-items-desktop__sidebar">
			{props.data.map((item, key) => {
				return <MessageItemDesktop 
					data={item} 
					isActive={Boolean(item.id === activeId)} 
					setActive={setActiveId}
					key={key}
				/>
			})}
		</div>
		<div className="message-items-desktop__body">
			{!activeItem
				? <div className="message-items-desktop__not-message">Выберите уведомление</div>
				: <div className="">
						<div className="message-item-desktop__header">
							<div className="message-item-desktop__owner-img">
								<img src={activeItem.image} alt="" />
							</div>
							<div className="message-item-desktop__header-wrap">
								<div className="message-item-desktop__header-head">
									<div className="message-item-desktop__owner">{activeItem.owner}</div>
									<div className="message-item-desktop__date">{activeItem.date}</div>
								</div>
							</div>
						</div>
						<div className="message-items-desktop__content">
							<div className="message-items-desktop__content-title">{activeItem.title}</div>
							<div className="message-items-desktop__content-desc">{activeItem.description}</div>
							<div className="message-items-desktop__btn-wrap">
								<Button type="primary" icon="rest" className="_black" onClick={() => { }}>Удалить</Button>
							</div>
						</div>
					</div>
			}
		</div>
	</div>
}

export default MessageItemsDesktop