import {Dispatch, SetStateAction} from 'react'
import { IMessageItem } from '../../pages/Messages/Messages.model'

export interface IMessageItemsDesktopProps {
	data: IMessageItem[]
}

export interface IMessageItemDesktopProps {
	data: IMessageItem
	setActive: Dispatch<SetStateAction<Number | null>>
	isActive: boolean
}