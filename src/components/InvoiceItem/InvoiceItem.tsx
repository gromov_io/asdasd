import React, { FC } from 'react'
import { IInvoiceItemProps } from './InvoiceItem.model'
import { Button } from 'antd'


export const InvoiceItem: FC<IInvoiceItemProps> = (props) => {
	const className = {
		status: `invoice-item__status _${props.data.status}`
	}

	return (
		<div className="invoice-item" onClick={() => props.onClick(true)}>
			<div className={className.status}></div>
			<div className="invoice-item__content">
				<div className="invoice-item__header">
					<div className="invoice-item__date">01.09.2019</div>
					<span className="invoice-item__sum">2 700,00 ₽</span>
				</div>
				<div className="invoice-item__body">
					<div className="invoice-item__text">
						<div className="invoice-item__desc">2 урока математики в сентябре в сентябре в сентябре</div>
						<div className="invoice-item__name">Иванов И. (2 класс)</div>
					</div>
					<button className="invoice-item__repeat-mobile"></button>
				</div>
			</div>
			<div className="invoice-item__content-desktop">
				<div className="invoice-item__header">
					<div className="invoice-item__date">01.09.2019</div>
				</div>
				<div className="invoice-item__body">
					<div className="invoice-item__desc">2 урока математики в сентябре в сентябре в сентябре</div>
					<div className="invoice-item__name">Иванов И. (2 класс)</div>
					<span className="invoice-item__sum">2 700,00 ₽</span>
					<Button className="invoice-item__repeat">Повторить</Button>
				</div>
			</div>
		</div>
	)
}

export default InvoiceItem