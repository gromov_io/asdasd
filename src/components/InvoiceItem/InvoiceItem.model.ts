
export interface IInvoiceItemData {
	status: 'success' | 'wait' | 'delete'
	date: string
	sum: number
	desc: string
	name: string
}

export interface IInvoiceItemProps {
	data: IInvoiceItemData
	onClick: (value: boolean) => void
}