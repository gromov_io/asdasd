import { FunctionComponent } from 'react'

export interface INavigationProps {
}

export interface INavigationLinkProps {
	url: string
	icon: FunctionComponent
	prefix: string
}

export interface IExternalLinkProps {
	url: string
	icon: FunctionComponent
	prefix: string
}

export interface INavigationItemProps {
	dispatch: ()=> void
	icon: FunctionComponent
	prefix: string
}