import React, { FC } from 'react'
import { INavigationProps, INavigationLinkProps, IExternalLinkProps, INavigationItemProps } from './Navigation.model'
import UserDefault from '~/assets/img/User-default.svg'
import { NavLink } from 'react-router-dom'
import NalogIcon from '~/assets/imgFC/Nalog'
import RubIcon from '~/assets/imgFC/Rub'
import InvoiceIcon from '~/assets/imgFC/Invoice'
import MessageIcon from '~/assets/imgFC/Message'
import WalletIcon from '~/assets/imgFC/Wallet'
import ExitIcon from '~/assets/imgFC/Exit'
import { IStore } from '~/store'
import { changeStatus } from '~/store/redux/modules/Navigation'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { changeSale } from '~/store/redux/modules/Sale'
import { $_Logout } from '~/store/redux/modules/Auth'

const mapStateNavigation = (state: IStore) => ({
	status: state.navigation.status
})

const mapStateMediaQuery = (state: IStore) => ({
	...state.mediaQuery
})

const mapStateProfile = (state: IStore) => ({
	...state.profile
})

const NavigationLink: FC<INavigationLinkProps> = (props) => {
	const dispatch = useDispatch()
	const MediaQuery = useSelector(mapStateMediaQuery, shallowEqual)

	const onClick = () => {
		if (MediaQuery.isMobile) {
			dispatch(changeSale(false))
			dispatch(changeStatus(false))
		}
	}

	return <NavLink exact to={props.url} onClick={onClick} className="navigation__item" activeClassName="_active">
		<div className="navigation__icon-wrap">
			<div className={`navigation__item-icon ${props.prefix}`} >
				<props.icon />
			</div>
		</div>
		<span className="navigation__item-text">
			{props.children}
		</span>
	</NavLink>
}

const NavigationItem: FC<INavigationItemProps> = (props) => {
	return <div onClick={() => props.dispatch()} className="navigation__item">
		<div className="navigation__icon-wrap">
			<div className={`navigation__item-icon ${props.prefix}`} >
				<props.icon />
			</div>
		</div>
		<span className="navigation__item-text">
			{props.children}
		</span>
	</div>
}

const ExternalLink: FC<IExternalLinkProps> = (props) => {
	return <a href={props.url} className="navigation__item _external">
		<div className="navigation__icon-wrap">
			<div className={`navigation__item-icon ${props.prefix}`} >
				<props.icon />
			</div>
		</div>
		<span className="navigation__item-text">
			{props.children}
		</span>
	</a>
}

export const Navigation: FC<INavigationProps> = (props) => {
	const { status } = useSelector(mapStateNavigation, shallowEqual)
	const MediaQuery = useSelector(mapStateMediaQuery, shallowEqual)
	const profile = useSelector(mapStateProfile, shallowEqual)
	const dispatch = useDispatch()

	const navigationClassName = 'navigation'
	const openSale = () => {
		dispatch(changeSale(profile.StatusSmz === 'Verified'))
	}

	const onClick = () => {
		if (MediaQuery.isMobile) {
			dispatch(changeSale(false))
			dispatch(changeStatus(false))
		}
	}

	const logout = () => {
		dispatch($_Logout())
	}

	const user = {
		get name() {
			const firstName = profile.UserAttributes.find(item => item.Name === 'FirstName')
			const lastName = profile.UserAttributes.find(item => item.Name === 'LastName')
			const Patronymic = profile.UserAttributes.find(item => item.Name === 'Patronymic')
			return `${lastName ? lastName.Value : ''} ${(firstName ? firstName.Value[0] : '') + '.'} ${Patronymic ? Patronymic.Value[0] : ''}`
		}
	}
	
	return (
		<div className={status ? navigationClassName : navigationClassName.concat(' _hide')}>
			<div className="navigation__hamburger"></div>
			<div className="navigation__wrap">
				<div className="navigation__header">
					<NavLink to="/account" onClick={onClick} className="navigation__user-wrap">
						<img className="navigation__user-image" src={UserDefault} alt="" />
						<span className="navigation__user-name">{user.name}</span>
					</NavLink>
					<div className="navigation__logout">
						<button className="navigation__logout-icon" onClick={logout}>
							<ExitIcon />
						</button>
					</div>
				</div>
				<NavigationLink url="/" icon={NalogIcon} prefix='_nalog'>Главная</NavigationLink>
				<NavigationItem dispatch={openSale} icon={RubIcon} prefix='_rub'>Новая продажа</NavigationItem>
				<NavigationLink url="/invoices" icon={InvoiceIcon} prefix='_invoice'>Операции</NavigationLink>
				<NavigationLink url="/messages" icon={MessageIcon} prefix='_message'>Уведомления</NavigationLink>
				<ExternalLink url="https://www.walletone.com/" icon={WalletIcon} prefix='_wallet'>Вывод средств</ExternalLink>
			</div>
		</div>
	)
}

export default Navigation