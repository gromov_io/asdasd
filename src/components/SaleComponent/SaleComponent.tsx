import React, { FunctionComponent, useState } from 'react'
import { Input, Radio, Button } from 'antd'
import { ISaleComponentProps, IPaymentsData, IPaymentsChangeData } from './SaleComponent.model'
import imgTypeOnline from '~/assets/img/Logo-PS.svg'

export const SaleComponent: FunctionComponent<ISaleComponentProps> = (props) => {
	const defaultSum = '0,00 ₽'

	const initialState: IPaymentsData = {
		type: 'online',
		card: '',
		name: '',
		sum: ''
	}

	const [paymentsData, setPaymentsData] = useState<IPaymentsData>(initialState)
	

	const className = {
		input(value:any) {
			const name = 'sale-component__input'
			return value ? name.concat(' _active') : name
		}
	}

	function onChange(val: IPaymentsChangeData) {
		const result = Object.assign(paymentsData, val)
		setPaymentsData({...result})
	}

	return (
		<div className="sale-component">
			<div className="sale-component__input-wrap">
				<Input className={className.input(paymentsData.card)} value={paymentsData.card} onChange={e => onChange({ card: e.target.value })} />
				<div className="sale-component__input__title">Номер кошелька</div>
			</div>
			<div className="sale-component__input-wrap">
				<Input className={className.input(paymentsData.name)} value={paymentsData.name} onChange={e => onChange({ name: e.target.value })} />
				<div className="sale-component__input__title">Название услуги</div>
			</div>
			<div className="sale-component__input-wrap _sum">
				<Input 
					className={className.input(paymentsData.sum).concat(' _sum')} 
					value={paymentsData.sum}
					placeholder={defaultSum}
					onChange={e => onChange({ sum: e.target.value })} 
				/>
				<div className="sale-component__input__title">Сумма</div>
			</div>

			<div className="sale-component__peyment-types">
				<Radio.Group size="large"  onChange={e => onChange({ type: e.target.value })} value={paymentsData.type}>
					<Radio value="online" className="sale-component__radio _online">Онлайн оплата</Radio>
					<div className="sale-component__type-desc">
						<img src={imgTypeOnline} alt="" />
						<div>и еще 43 способа</div>
					</div>
					<Radio value="isCash" className="sale-component__radio">Оплата наличными</Radio>
					<Radio value="olds" className="sale-component__radio">Другой способ оплаты</Radio>
				</Radio.Group>
			</div>
			<div className="sale-component__btn-wrap">
				<Button type="primary" className="_default home__btn">Получить чек</Button>
			</div>
		</div>
	)
}

export default SaleComponent