
export interface ISaleComponentProps { }

export interface IPaymentsData {
	type: PaymentsType
	card: string
	name: string
	sum: string
}

export interface IPaymentsChangeData {
	card?: string
	name?: string
	sum?: string
	type?: PaymentsType
}

export type PaymentsType = 'online' | 'olds' | 'isCash'
