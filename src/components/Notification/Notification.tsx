import React, { FC, useState} from 'react'
import { INotificationProps } from './Notification.model'
import NotificationIconNo from '~/assets/imgFC/NotificationNo'
import NotificationIconYes from '~/assets/imgFC/Notification'

export const Notification: FC<INotificationProps> = (props) => {
	const [status, setStatus] = useState(false)
	
	const NotificationIcon = status 
		? <button className="notification__icon">
			<NotificationIconYes />
		</button> 
		: <button className="notification__icon _no">
			<NotificationIconNo />
		</button> 
	
	return (
		<div className="notification" onClick={() => setStatus(!status)}>
			<div className="notification__icon-wrap">
				{ NotificationIcon }
			</div>
		</div>
	)
}

export default Notification