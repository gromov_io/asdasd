import React, { FC, useState } from 'react'
import { IMessageItemsMobileProps, IMessageItemMobileProps } from './MessageItemsMobile.model'
import { Button } from 'antd'

const MessageItemMobile: FC<IMessageItemMobileProps> = (props) => {
	const [isOpen, setOpen] = useState<boolean>(false)

	const className = {
		get item() {
			const name = 'message-item'
			return isOpen ? name.concat(' _open') : name
		}
	}

	return <div className={className.item} onClick={() => setOpen(!isOpen)}>
		<div className="message-item__header">
			<div className="message-item__owner-img">
				<img src={props.data.image} alt="" />
			</div>
			<div className="message-item__header-wrap">
				<div className="message-item__header-head">
					<div className="message-item__owner">{props.data.owner}</div>
					<div className="message-item__date">12.10.2019</div>
				</div>
				<div className="message-item__title">{props.data.title}</div>
			</div>
		</div>
		<div className="message-item__body">
			<div className="message-item__body-title">{props.data.title}</div>
			<div className="message-item__body-desc">{props.data.description}</div>
			<div className="message-item__btn-wrap">
				<Button type="primary" icon="rest" className="_black" onClick={() => { }}>Удалить</Button>
			</div>
		</div>
	</div>
}

export const MessageItemsMobile: FC<IMessageItemsMobileProps> = (props) => {
	return <div>
		{props.data.map((item, key) => {
			return <MessageItemMobile data={item} key={key}/>
		})}
	</div>
}

export default MessageItemsMobile