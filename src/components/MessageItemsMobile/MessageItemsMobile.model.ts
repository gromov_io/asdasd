import { IMessageItem } from '../../pages/Messages/Messages.model'

export interface IMessageItemsMobileProps {
	data: IMessageItem[]
}

export interface IMessageItemMobileProps {
	data: IMessageItem
}