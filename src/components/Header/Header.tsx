import { Link } from 'react-router-dom'
import React, { FC } from 'react'
import { IHeaderProps } from './Header.model'
import { Hamburger } from '~/components/Hamburger/Hamburger'
import { Notification } from '~/components/Notification/Notification'

export const Header: FC<IHeaderProps> = () => {
	return <div className="header">
		<Link to="/" className="header__logo">Кошелек <br/> самозанятого</Link>
		<div className="header__right-panel">
			<Notification/>
			<Hamburger prefix="_header" />
		</div>
	</div>
}

export default Header