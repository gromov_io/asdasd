import { IAuthData } from '~/layouts/Login/Login.model'
import axios from '~/plugins/axios'

export function $_getToken(param: IAuthData) {
	return axios({
		url: `/oauth/${param.code}`,
		method: 'get'
	})
}