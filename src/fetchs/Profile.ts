import axios from '~/plugins/axios'

export function $_getProfile(inn?:string) {
	return axios({
		baseURL: 'http://localhost:3001/api/v1',
		url: `/profile`,
		method: 'get'
	})
}
