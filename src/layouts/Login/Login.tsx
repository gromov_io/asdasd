import React, { FunctionComponent, useEffect } from 'react'
import { ILoginProps } from './Login.model'
import W1Logo from '~/assets/img/w1-login-logo.svg'
import { Spin, Icon } from 'antd';
import { useDispatch, useSelector, shallowEqual } from 'react-redux'
import {  $_Authorization } from '~/store/redux/modules/Auth'
import { IStore } from '~/store'

const mapStateAuth = (state: IStore) => ({
	storeAuth: state.auth
})

export const Login: FunctionComponent<ILoginProps> = (props) => {
	const dispatch = useDispatch()
	const { storeAuth } = useSelector(mapStateAuth, shallowEqual)

	useEffect(() => {
		dispatch($_Authorization())
	}, [])


	return <div className="login-page">	
		<div className="login-page__header">
			<div className="login-page__logo">Кошелек <br /> самозанятого</div>
		</div>
		
		{storeAuth.loading
			?
				<div className="login-page__auth _login">
					<span className="login-page__load-text">Загружаю...</span>
					<Spin indicator={<Icon type="loading" style={{ fontSize: 54 }} spin />} />
				</div>
			:
				<div className="login-page__auth">
					<div className="login-page__auth-title">Войти через</div>
					<img src={W1Logo} alt="" />
					<div className="login-page__auth-desc">Единый Кошелек</div>
					<a href="https://api.w1.ru/oauth2/authorize?client_id=npd&scope=All&response_type=code" className="login-page__auth-btn">Войти</a>
				</div>
		}

	</div>
}

export default Login