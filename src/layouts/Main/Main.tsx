import React, { FC, useEffect } from 'react'
import { IMainProps } from './model'
import { Header } from '~/components/Header/Header'
import { Navigation } from '~/components/Navigation/Navigation'
import Routes from '~/routes'
import { useSelector, shallowEqual, useDispatch } from 'react-redux'
import { IStore } from '~/store'
import { Modal, Spin } from 'antd'
import { SaleComponent } from '~/components/SaleComponent/SaleComponent'
import { changeSale } from '~/store/redux/modules/Sale'
import { $_updateProfile } from '~/store/redux/modules/Profile'

const mapStateSaleModal = (state: IStore) => ({
	sale: state.sale
})

const mapStateProfile = (state: IStore) => ({
	...state.profile
})


export const Main:FC<IMainProps> = () => {
	const { sale } = useSelector(mapStateSaleModal, shallowEqual)
	const profile = useSelector(mapStateProfile, shallowEqual)
	const dispatch = useDispatch()

	useEffect(function () {
		dispatch($_updateProfile())
	}, [])

	const close = () => {
		dispatch(changeSale(false))
	}
	
	return (
		<Spin spinning={profile.loading} tip="Загружаю профайл...">
			<div className="main">
					<div className="main__content">
						<Header />
						{profile.StatusSmz === 'Verified'
							? <Routes />
							: <div className="page">Ваш статус не является <strong>Verified</strong></div>
						}
						<div className="footer"></div>
					</div>
					<Navigation />

					<Modal
						wrapClassName="sale__modal"
						title={null}
						visible={sale.status}
						onOk={() => { }}
						onCancel={close}
						footer={null}
					>
						<div className="sale__title">Новая продажа</div>
						<SaleComponent />
					</Modal>
			</div>
		</Spin>
	)
}

export default Main