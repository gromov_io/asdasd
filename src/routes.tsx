import React, { FC } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home/Home'
import Invoices from './pages/Invoices/Invoices'
import Messages from './pages/Messages/Messages'
import Account from './pages/Account/Account'

export const Routes:FC = ()=> {
	return (
		<Switch>
			<Route exact path="/" component={Home} />
			<Route path="/invoices" component={Invoices} />
			<Route path="/messages" component={Messages} />
			<Route path="/account" component={Account} />
		</Switch>
	)
}

export default Routes