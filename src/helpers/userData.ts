import { IAuthData } from '~/layouts/Login/Login.model'
import { IUserProfile } from '~/store/redux/modules/Profile'
import qs from 'qs'

export interface IUserDataLocalStorage {
	token: string
	userId: string
}

export function saveUserData(token:string, userId:string):void {
	localStorage.setItem('npd_userId', userId)
	localStorage.setItem('npd_token', token)
}

export function removeUserData() {
	localStorage.removeItem('npd_userId')
	localStorage.removeItem('npd_token')
}

export function getUserData(): IUserDataLocalStorage {
	const token = localStorage.getItem('npd_token') || ''
	const userId = localStorage.getItem('npd_userId') || ''
	return { token, userId }
}

export function AuthData(): IAuthData {
	const locationSearchString = qs.parse(window.location.search.slice(1))
	const result: IAuthData = {
		code: locationSearchString.code || '',
		user_id: locationSearchString.user_id || ''
	}

	return result
}

export function UserDefaultProfile(loading: boolean = false): IUserProfile {
	return {
		Id: '',
		Inn: '',
		StatusSmz: 'NotIdentified',
		StatusIdentification: 'NoIdentification',
		ExternalUserId: 'string',
		ExternalSystemId: 'w1',
		RegisteredByUs: false,
		PlutoniumUserId: null,
		UserAttributes: [],
		loading: loading
	}
}