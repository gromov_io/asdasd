import { createStore, compose, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk'
import { rootReducer } from '~/store/redux'
//modules
import { INavigationState } from './redux/modules/Navigation'
import { IMediaQueryState } from './redux/modules/MediaQuery'
import { ISaleState } from './redux/modules/Sale'
import { IAuthState } from './redux/modules/Auth'
import { IUserProfile } from './redux/modules/Profile'

let composeEnhancers = compose
const middlewares = [
	ReduxThunk
]

if (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
	composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
}

export interface IStore {
	navigation: INavigationState
	mediaQuery: IMediaQueryState
	sale: ISaleState
	auth: IAuthState
	profile: IUserProfile
}


const configureStore = (initialState?: IStore) => {
	return createStore(
		rootReducer,
		initialState,
		composeEnhancers(
			applyMiddleware(...middlewares)
		)
	)
}

export default configureStore