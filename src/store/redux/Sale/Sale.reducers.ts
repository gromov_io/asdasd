import { ISaleState, CHANGE_SALE_STATUS, ISaleActions } from './Sale.model'

const initialState: ISaleState = {
	status: false
}

export function SaleReducer(state = initialState, action: ISaleActions): ISaleState {
	switch (action.type) {
		case CHANGE_SALE_STATUS:
			return action.payload
		default:
			return state
	}
}

export default SaleReducer