import { IActions } from '../../model'
export const CHANGE_SALE_STATUS = 'CHANGE_SALE_STATUS'


export interface ISaleState {
	status: boolean
}

export interface IChangeSaleActions {
	status: boolean
}

export type ISaleActions = IActions<typeof CHANGE_SALE_STATUS, IChangeSaleActions>