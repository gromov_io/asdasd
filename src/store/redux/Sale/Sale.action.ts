import { CHANGE_SALE_STATUS } from './Sale.model'

export const changeSale = (status: boolean) => ({
	type: CHANGE_SALE_STATUS,
	payload: { status }
})