import { IUserState, CHANGE_USER_STATUS, IUserActions } from './User.model'

const initialState: IUserState = {
	status: false
}

export function UserReducer(state = initialState, action: IUserActions): IUserState {
	switch (action.type) {
		case CHANGE_USER_STATUS:
			return action.payload
		default:
			return state
	}
}

export default UserReducer