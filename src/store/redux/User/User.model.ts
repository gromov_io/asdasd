import { IActions } from '~/store/model'

export const CHANGE_USER_STATUS = 'CHANGE_USER_STATUS'

export interface IUserState {
	status: boolean
}

export interface IChangeUserActions {
	status: boolean
}

export type IUserActions = IActions<typeof CHANGE_USER_STATUS, IChangeUserActions>