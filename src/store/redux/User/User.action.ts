import { CHANGE_USER_STATUS } from './User.model'

export const changeSale = (status: boolean) => ({
	type: CHANGE_USER_STATUS,
	payload: { status }
})