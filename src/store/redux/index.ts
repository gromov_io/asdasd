import { combineReducers } from 'redux'
import { NavigationReducer } from './modules/Navigation'
import { MediaQueryReducer } from './modules/MediaQuery'
import { SaleReducer } from './modules/Sale'
import { AuthReducer } from './modules/Auth'
import { ProfileReducer } from './modules/Profile'
import { IStore } from '../'

const Reducers = {
	navigation: NavigationReducer,
	mediaQuery: MediaQueryReducer,
	sale: SaleReducer,
	auth: AuthReducer,
	profile: ProfileReducer
}

export const rootReducer = combineReducers<IStore>(Reducers)

