
export const NAVIGATION_CHANGE_STATUS = 'NAVIGATION_CHANGE_STATUS'

export interface INavigationState {
	status: boolean
}