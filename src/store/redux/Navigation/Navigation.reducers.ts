import { INavigationState } from './Navigation.model'
import { NAVIGATION_CHANGE_STATUS } from './Navigation.model'

const initialState: INavigationState = {
	status: false
}

export function NavigationReducer(state = initialState, action: any): INavigationState {
	switch (action.type) {
		case NAVIGATION_CHANGE_STATUS:
			return { status: action.payload }
		default:
			return { status: false }
	}
}

export default NavigationReducer