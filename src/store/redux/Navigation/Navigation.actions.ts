import { NAVIGATION_CHANGE_STATUS } from './Navigation.model'

export const changeStatus = (status:boolean) => ({
	type: NAVIGATION_CHANGE_STATUS,
	payload: status
})