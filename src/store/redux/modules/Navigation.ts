import { IActions } from '~/store/model'

export const NAVIGATION_CHANGE_STATUS = 'NAVIGATION_CHANGE_STATUS'

export interface INavigationState {
	status: boolean
}

const initialState: INavigationState = {
	status: false
}

export type INavigationAction = IActions<typeof NAVIGATION_CHANGE_STATUS, boolean >
export type INavigationActions = INavigationAction

export const changeStatus = (status:boolean) => ({
	type: NAVIGATION_CHANGE_STATUS,
	payload: status
})

// Reducers

export function NavigationReducer(state = initialState, action: INavigationActions): INavigationState {
	switch (action.type) {
		case NAVIGATION_CHANGE_STATUS:
			return { status: action.payload }
		default:
			return { status: false }
	}
}

export default NavigationReducer