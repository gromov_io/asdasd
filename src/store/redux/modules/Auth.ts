import { $_getToken } from '~/fetchs/Auth'
import { saveUserData, removeUserData, getUserData } from '~/helpers/userData'
import { AuthData } from '~/helpers/userData'
import { IActions } from '~/store/model'
import { notification } from 'antd'


export const CHANGE_AUTH_DATA = 'CHANGE_AUTH_DATA'
export const CHANGE_AUTH_LOADING = 'CHANGE_AUTH_LOADING'
export const LOGOUT = 'LOGOUT'

export interface IAuthState {
	authorized: boolean
	loading: boolean
	token: string
	userId: string
}

export interface IChangeAuthData extends IAuthState {}
export interface ILogoutData extends IAuthState {}

export type IChangeAuthDataAction = IActions<typeof CHANGE_AUTH_DATA, IChangeAuthData>
export type ILogoutAction = IActions<typeof LOGOUT, ILogoutData>
export type IChangeAuthLoaderAction = IActions<typeof CHANGE_AUTH_LOADING, {loading: boolean}>

export type IAuthActions = IChangeAuthDataAction | IChangeAuthLoaderAction | ILogoutAction

// ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
//  Actions
// ________________
export const changeAuthData = (data: IChangeAuthData): IChangeAuthDataAction => ({
	type: CHANGE_AUTH_DATA,
	payload: data
})

export const changeAuthLoader = (loading: boolean): IChangeAuthLoaderAction => ({
	type: CHANGE_AUTH_LOADING,
	payload: { loading }
})

export const LogoutAction = (): ILogoutAction => ({
	type: LOGOUT,
	payload: {
		loading: false,
		authorized: false,
		userId: '',
		token: '',
	}
})

function errorHandler(dispatch:any) {
	removeUserData()
	dispatch(changeAuthLoader(false))
	notification.error({
		message: 'Ошибка',
		description: 'Сервер авторизации вернул недопустимый ответ, пожалуйста обратитесь к администратору за помощью',
	})
}

function sessionHandler(dispatch:any) {
	removeUserData()
	dispatch(changeAuthLoader(false))
	notification.info({
		message: 'Извините..',
		description: 'Время сессии истекло, требуется повторная авторизация',
	})
}

export function $_Authorization() {
	return async (dispatch: any) => {
		const { code, user_id } = AuthData()
		const needAuth = Boolean(code && user_id)
		
		if (needAuth) {
			try {
				const { data } = await $_getToken({ code, user_id })
				const token = data
				if (typeof token === 'string') {
					const result = {
						authorized: Boolean(token && user_id),
						loading: false,
						userId: user_id,
						token: token
					}
					saveUserData(token, user_id)
					window.history.replaceState({}, 'login-page', '/')
					dispatch(changeAuthData(result))
				} else {
					errorHandler(dispatch)
				}
			} catch (error) {
				sessionHandler(dispatch)
			}
		}
	}
}

export function $_Logout () {
	return async (dispatch: any) => {
		removeUserData()
		dispatch(LogoutAction())
	}
}

// ‾‾‾‾‾‾‾‾‾‾‾‾‾
//  Reducer
// ________________

const { code, user_id } = AuthData()
const { token, userId } = getUserData()

const initialState: IAuthState = {
	loading: Boolean(code && user_id),
	authorized: Boolean(token && userId),
	userId,
	token,
}

export function AuthReducer(state = initialState, action: IAuthActions): IAuthState {
	switch (action.type) {
		case CHANGE_AUTH_DATA:
			return action.payload
		case LOGOUT:
			return action.payload
		case CHANGE_AUTH_LOADING:
			return Object.assign({}, state, action.payload)
		default:
			return state
	}
}

export default AuthReducer