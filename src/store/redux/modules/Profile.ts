import { IActions } from '~/store/model'
import { UserDefaultProfile } from '~/helpers/userData'
import { $_getProfile } from '~/fetchs/Profile'

// Models

const SET_USER_PROFILE = 'SET_USER_PROFILE'
const CHANGE_PROFILE_LOADER = 'CHANGE_PROFILE_LOADER'

export interface IAuthUserAttributes {
  Name: string,
  Value: string,
  VerificationStatus: string
}

export interface IUserProfile {
  Id: string
  Inn: string
  StatusSmz: 'IsSmz' | 'New' | 'NotIdentified' | 'NotSmz' | 'Verified' | 'WaitingConfirmation'
  StatusIdentification: 'Full' | 'NoIdentification'
  ExternalUserId: string
  ExternalSystemId: 'w1'
  RegisteredByUs: boolean
  PlutoniumUserId: null | string
  UserAttributes: IAuthUserAttributes[]
	loading: boolean
}

export type IupdateProfileAction = IActions<typeof SET_USER_PROFILE, IUserProfile>
export type IchangeProfileLoaderAction = IActions<typeof CHANGE_PROFILE_LOADER, {loading: boolean}>
export type IProfileActions = IupdateProfileAction | IchangeProfileLoaderAction

// Actions

export function updateProfile(profile: IUserProfile): IupdateProfileAction {
  return {
    type: SET_USER_PROFILE,
    payload: profile
  }
}

export function changeProfileLoader(loading: boolean): IchangeProfileLoaderAction {
  return {
    type: CHANGE_PROFILE_LOADER,
    payload: { loading }
  }
}

export function $_updateProfile() {
	return async (dispatch: any, state: any) => {
		const { profile } = state()

		if (!profile.loading) dispatch(changeProfileLoader(true))

		try {
			const { data } = await $_getProfile()
			dispatch(updateProfile(data))
		} catch(err)  {}
		dispatch(changeProfileLoader(false))
	}
}

// Reducers
const initialState: IUserProfile = UserDefaultProfile(true)

export function ProfileReducer(state = initialState, action: IProfileActions) {
  switch (action.type) {
	  case CHANGE_PROFILE_LOADER:
    	return Object.assign({}, state, action.payload)
    case SET_USER_PROFILE:
      return action.payload
    default:
      return state
  }
}