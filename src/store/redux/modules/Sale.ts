import { IActions } from '~/store/model'

export const CHANGE_SALE_STATUS = 'CHANGE_SALE_STATUS'

export interface ISaleState {
	status: boolean
}

export interface IChangeSaleActions {
	status: boolean
}

export const changeSale = (status: boolean) => ({
	type: CHANGE_SALE_STATUS,
	payload: { status }
})

export type ISaleActions = IActions<typeof CHANGE_SALE_STATUS, IChangeSaleActions>

const initialState: ISaleState = {
	status: false
}

export function SaleReducer(state = initialState, action: ISaleActions): ISaleState {
	switch (action.type) {
		case CHANGE_SALE_STATUS:
			return action.payload
		default:
			return state
	}
}

export default SaleReducer