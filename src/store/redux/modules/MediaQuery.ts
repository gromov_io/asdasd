import { IActions } from '~/store/model'


export const CHANGE_MEDIA = 'CHANGE_MEDIA'

export interface IMediaQueryState {
	isMobile: boolean
	isTable: boolean
	isDesktop: boolean
}

export interface IChangeMediaQueryAction {
	isMobile?: boolean
	isTable?: boolean
	isDesktop?: boolean
}

export type IMediaQueryActions = IActions<typeof CHANGE_MEDIA, IChangeMediaQueryAction>


export const changeMediaQuery = (MediaQuery: IChangeMediaQueryAction) => ({
	type: CHANGE_MEDIA,
	payload: MediaQuery
})

const initialState: IMediaQueryState = {
	isMobile: false,
	isTable: false,
	isDesktop: false
}

export function MediaQueryReducer(state = initialState, action: IMediaQueryActions): IMediaQueryState {
	switch (action.type) {
		case CHANGE_MEDIA:
			return Object.assign({}, state, action.payload)
		default:
			return state
	}
}

export default MediaQueryReducer