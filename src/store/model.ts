

export interface IActions<T, P> {
	type: T
	payload: P
}