const express = require('express')
const app = express()
const port = 3001

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin', req.headers.origin)
	res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
	next()
})

const isSmzUserFullId = require('./mockup/isSmzUserFullId.json') 
const isSmzUserNoId = require('./mockup/isSmzUserNoId.json')

app.get('/api/v1/profile', (req, res) => res.send(isSmzUserFullId))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))